# docker-wordpress-ubuntu16-nginx-php7

A Dockerfile that installs the latest wordpress on Ubuntu 16.04 with nginx 1.10.0, php-fpm7.0, php7.0 APC User Cache and openssh. You can also handle the services using supervisord.
Based on [this](https://hub.docker.com/r/thomasvan/docker-wordpress-ubuntu16-nginx-php7/).

## Installation

If you'd like to build the image yourself:

```bash
$ git clone https://github.com/thomasvan/docker-wordpress-ubuntu16-nginx-php7.git
$ cd docker-wordpress-nginx-ssh
$ sudo docker build -t wordpress .
```

## Usage

Set the following environment variables:

| Name               | Purpose                                                                                             |
|--------------------|-----------------------------------------------------------------------------------------------------|
| WORDPRESS_DB       | MySQL schema name                                                                                   |
| WORDPRESS_HOST     | Fully qualified domain name of the MySQL host                                                       |
| WORDPRESS_USER     | MySQL username providing access to the schema on the host                                           |
| WORDPRESS_PASSWORD | MySQL user's password                                                                               |
| SITE_ADDRESS       | Fully qualified domain name where the site will be offered from - used to set up Nginx virtual host |

The -p 80:80 maps the internal docker port 80 to the outside port 80 of the host machine. 
The -p 9011:9011 is using for supervisord, listing out all services status. 

```bash
$ sudo docker run \
-p 8080:80 -p 9011:9011 \
--name docker-name \
-e "WORDPRESS_DB=wordpress_onevigor" \
-e "WORDPRESS_HOST=corporate.c75h8kdzv9hg.us-west-2.rds.amazonaws.com" \
-e "WORDPRESS_USER=wp_onevigor" \
-e "WORDPRESS_PASSWORD=W4p@6NkM@^8IpXSLW7" \
-e "SITE_ADDRESS=beta.onevigor.tv" \
-d wordpress:latest
```

Start your newly created container, named *docker-name*.

```
$ sudo docker start docker-name
```

After starting the container docker-wordpress-nginx-ssh checks to see if it has started and the port mapping is correct.  This will also report the port mapping between the docker container and the host machine.

```
$ sudo docker ps

3306/tcp, 0.0.0.0:9011->9011/tcp, 0.0.0.0:8080->80/tcp
```

You can then visit the following URL in a browser on your host machine to get started:

```
http://127.0.0.1:8080
```

You can start/stop/restart and view the error logs of nginx and php-fpm services:
```
http://127.0.0.1:9011
```
