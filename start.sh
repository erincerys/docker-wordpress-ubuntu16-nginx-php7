#!/bin/bash

if [ ! -f /usr/share/nginx/www/wp-config.php ]; then

    sed -e "s/database_name_here/$WORDPRESS_DB/
    s/username_here/$WORDPRESS_USER/
    s/password_here/$WORDPRESS_PASSWORD/
    /'DB_HOST'/s/localhost/$WORDPRESS_HOST/
    /'AUTH_KEY'/s/put your unique phrase here/`pwgen -c -n -1 65`/
    /'SECURE_AUTH_KEY'/s/put your unique phrase here/`pwgen -c -n -1 65`/
    /'LOGGED_IN_KEY'/s/put your unique phrase here/`pwgen -c -n -1 65`/
    /'NONCE_KEY'/s/put your unique phrase here/`pwgen -c -n -1 65`/
    /'AUTH_SALT'/s/put your unique phrase here/`pwgen -c -n -1 65`/
    /'SECURE_AUTH_SALT'/s/put your unique phrase here/`pwgen -c -n -1 65`/
    /'LOGGED_IN_SALT'/s/put your unique phrase here/`pwgen -c -n -1 65`/
    /'NONCE_SALT'/s/put your unique phrase here/`pwgen -c -n -1 65`/" /usr/share/nginx/www/wp-config-sample.php > /usr/share/nginx/www/wp-config.php

    # Download nginx helper plugin
    curl -O `curl -i -s https://wordpress.org/plugins/nginx-helper/ | egrep -o "https://downloads.wordpress.org/plugin/[^\"]+"`
    unzip -o nginx-helper.* -d /usr/share/nginx/www/wp-content/plugins

    # Activate nginx plugin and set up pretty permalink structure once logged in
    cat << ENDL >> /usr/share/nginx/www/wp-config.php
# Force SSL and set Vhost for site
define('WP_SITEURL','https://$SITE_ADDRESS');
define('WP_HOME','https://$SITE_ADDRESS');
if (strpos(\$_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') !== false)
  \$_SERVER['HTTPS']='on';

\$plugins = get_option( 'active_plugins' );
if ( count( \$plugins ) === 0 ) {
  require_once(ABSPATH .'/wp-admin/includes/plugin.php');
  \$wp_rewrite->set_permalink_structure( '/%postname%/' );
  \$pluginsToActivate = array( 'nginx-helper/nginx-helper.php' );
  foreach ( \$pluginsToActivate as \$plugin ) {
    if ( !in_array( \$plugin, \$plugins ) ) {
      activate_plugin( '/usr/share/nginx/www/wp-content/plugins/' . \$plugin );
    }
  }
}
ENDL

    chown -R wordpress: /usr/share/nginx/www/

fi

if [ `grep -c $SITE_ADDRESS /etc/nginx/sites-available/default` -ne 0 ] ; then
  # set nginx virtualhosts according to env
  PRIVATE_IP=`curl http://169.254.169.254/latest/meta-data/local-ipv4`
  sed -i -e "s/___VIRTUALHOST___/${SITE_ADDRESS}/g" /etc/nginx/sites-available/default
fi

# start all the services
/usr/local/bin/supervisord -n -c /etc/supervisord.conf
